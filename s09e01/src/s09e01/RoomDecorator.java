/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s09e01;

/**
 *
 * @author M15633
 */
public class RoomDecorator implements Room{
    protected final Room room;
    
    public RoomDecorator (Room room) {
        this.room = room;
    }
    
    @Override
    public void displayRoom () {
        room.displayRoom();
    }
}
