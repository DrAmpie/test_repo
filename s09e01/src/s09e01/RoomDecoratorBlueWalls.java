/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s09e01;

/**
 *
 * @author M15633
 */
public class RoomDecoratorBlueWalls extends RoomDecorator{
    
    public RoomDecoratorBlueWalls(Room room) {
        super(room);
    }
    
    @Override
    public void displayRoom () {
        room.displayRoom();
        paintBlue(room);
    }
    
    private void paintBlue (Room room) {
        System.out.println("walls colour: pink");
    }
}
