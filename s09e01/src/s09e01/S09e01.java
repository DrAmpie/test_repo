/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s09e01;

/**
 *
 * @author M15633
 */
public class S09e01 {

    public static void main(String[] args) {
        Room bedroom        = new Bedroom();
        Room kitchen        = new Kitchen();
        
        Room pinkBedroom    = new RoomDecoratorBlueWalls(bedroom);
        Room pinkBedroom2    = new RoomDecoratorBlueWalls(pinkBedroom);
        Room pinkKitchen    = new RoomDecoratorBlueWalls(new Kitchen());
        
        System.out.println("~~ Basic rooms: ");
        bedroom.displayRoom();
        kitchen.displayRoom();
        
        System.out.println("~~ A decorated bedroom: ");
        pinkBedroom.displayRoom();
        
        System.out.println("~~ A decorated kitchen: ");
        pinkKitchen.displayRoom();
        
        System.out.println("~~ A decorated bedroom2: ");
        pinkBedroom2.displayRoom();
        
    }
    
}
